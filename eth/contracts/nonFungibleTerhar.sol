// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "hardhat/console.sol";

import { Base64 } from "./libraries/Base64.sol";

contract NonFungibleTerhar is ERC721URIStorage {
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;
  string baseSvg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<svg width=\"545px\" height=\"417px\" viewBox=\"0 0 545 417\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\"> <style>\n    .heavy { font: bold 50px sans-serif; fill: orange; stroke: white }\n    .shadow { font: bold 50px sans-serif; fill: gray; stroke: black }\n   .dynamic { font: bold 30px sans-serif; fill: black; stroke: black } </style>\n <rect width='100%' height='100%' fill='lightblue' />\n <rect width='100%' y='78%' height='20%' fill='lightgreen' />\n <path id=\"Path\" d=\"M150 402 L153.378 395.327 C153.378 385.053 178.98 352.862 184.287 339.603 208.804 278.332 208.364 286.764 215 217 M217 160 C218.949 134.67 226.442 95.516 194 77 160.704 57.996 102.823 79.135 94.522 116.041 78.911 185.446 148.224 185.219 190.459 172.462 212.583 165.78 252.329 144.212 272.865 134.121 309.495 116.123 353.45 93.012 389 73\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-1\" d=\"M296.77 395.463 L292.012 391.624 C237.022 347.24 255.296 251.933 261 190 M262 138 C261.542 89.118 261.441 59.446 212 32 167.792 7.465 97.033 25.172 69.849 64.682 44.608 101.371 34.543 170.951 68 205 145.054 283.409 251.944 188.002 336 150 363.462 137.584 382.045 130.183 407.248 113.34\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-2\" d=\"M409 113 L410.673 128.72 C407.863 142.39 409.19 145.664 405 159\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-3\" d=\"M389.143 71.83 L382 70 C373.644 67.735 371.199 67.402 361 64 357.05 62.683 337.932 58.125 328 54\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-4\" d=\"M488 52 L478.606 64.977 C453.368 94.136 433.458 132.779 405 159\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-5\" d=\"M328.799 54.032 L332.347 54.032 C375.148 54.032 417.302 51.264 460 51 469.26 50.943 478.924 50.691 488.169 50.976\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-6\" d=\"M150.749 403.268 L153.445 403.268 C199.349 403.268 245.062 397.003 291 397 292.949 397 294.182 396.915 296.409 396.654\" fill=\"none\" stroke=\"#ffffff\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <use id=\"Layer-1\" xlink:href=\"#image-1\" x=\"0px\" y=\"0px\" width=\"545px\" height=\"417px\"/>\n <path id=\"Path-7\" d=\"M190.634 324.117 L188.324 324.117 C127.79 324.117 67.456 329.525 6.945 329.604 4.181 329.608 1.405 329.452 -1.363 329.446\" fill=\"none\" stroke=\"#ffffff\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n <path id=\"Path-8\" d=\"M256.716 321.66 L260.147 321.66 C341.091 321.66 422.404 324.229 503.306 321.899 517.824 321.48 532.122 318.519 546.66 318.472\" fill=\"none\" stroke=\"#ffffff\" stroke-width=\"3\" stroke-opacity=\"1\" stroke-linejoin=\"round\" stroke-linecap=\"round\"/>\n  <text x=\"50%\" y=\"22%\" class=\"shadow\" dominant-baseline=\"middle\" text-anchor=\"middle\">Nonfungible Terhar</text>\n <text x=\"49.7%\" y=\"21.3%\" class=\"heavy\" dominant-baseline=\"middle\" text-anchor=\"middle\">Nonfungible Terhar</text>\n<text x=\"50%\" y=\"62%\" class=\"dynamic\" dominant-baseline=\"middle\" text-anchor=\"middle\">";
  
  string[] firstWords = ["Captain ", "Mister ",     "Otherwise ",  "Doctor ", "Madam ",  "The great "];
  string[] secondWords = ["formal ", "underwater ", "deep fried ", "albino ", "primal ", "adorable "];
  string[] thirdWords = ["fungus",   "pearl",       "windmill",    "horn",    "grouch",  "lump"];

  event NewNFTerharMinted(address sender, uint256 tokenId);

  constructor() ERC721 ("SquareNFT", "SQUARE") {
    console.log("This Terhar is never going to Funge!");
  }

  function pickRandomFirstWord(uint256 tokenId) public view returns (string memory) {
    uint256 rand = random(string(abi.encodePacked("FIRST_WORD", Strings.toString(tokenId))));
    rand = rand % firstWords.length;
    return firstWords[rand];
  }

  function pickRandomSecondWord(uint256 tokenId) public view returns (string memory) {
    uint256 rand = random(string(abi.encodePacked("SECOND_WORD", Strings.toString(tokenId))));
    rand = rand % secondWords.length;
    return secondWords[rand];
  }

  function pickRandomThirdWord(uint256 tokenId) public view returns (string memory) {
    uint256 rand = random(string(abi.encodePacked("THIRD_WORD", Strings.toString(tokenId))));
    rand = rand % thirdWords.length;
    return thirdWords[rand];
  }

  function random(string memory input) internal pure returns (uint256) {
      return uint256(keccak256(abi.encodePacked(input)));
  }

  function makeNFTerhar() public {
    uint256 newItemId = _tokenIds.current();

    string memory first = pickRandomFirstWord(newItemId);
    string memory second = pickRandomSecondWord(newItemId);
    string memory third = pickRandomThirdWord(newItemId);
    string memory combinedWord = string(abi.encodePacked(first, second, third));

    string memory finalSvg = string(abi.encodePacked(baseSvg, combinedWord, "</text></svg>"));

    string memory json = Base64.encode(
        bytes(
            string(
                abi.encodePacked(
                    '{"name": "',
                    combinedWord,
                    ' by NonFungibleTerhar", "description": "There is no funging around.", "image": "data:image/svg+xml;base64,',
                    Base64.encode(bytes(finalSvg)),
                    '"}'
                )
            )
        )
    );

    string memory finalTokenUri = string(
        abi.encodePacked("data:application/json;base64,", json)
    );

    console.log("\n--------------------");
    console.log(finalSvg);
    console.log("--------------------\n");

    _safeMint(msg.sender, newItemId);
    
    _setTokenURI(newItemId, finalTokenUri);
    console.log("NonFungibleTerhar with ID %s has been minted to %s", newItemId, msg.sender);
    _tokenIds.increment();
    emit NewNFTerharMinted(msg.sender, newItemId);
  }
}