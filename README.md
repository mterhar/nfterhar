A project to explore NFTs with crappy art

From the [buildspace NFT course](https://app.buildspace.so/courses/CO961ddb5f-f428-4608-9949-a9a2f461eb3f). 

### Outputs for Section 1.

1. The source code at this commit
2. This [imgur picture](https://i.imgur.com/CpqOyW3.jpg)
3. This [JSON thing](https://jsonkeeper.com/b/7GW3)
4. This [testnet NFT collection](https://testnets.opensea.io/collection/squarenft-vt2vcc2iy6)

### Outputs for Section 2.

1. The contract mints a pretty picture
2. It's all in [the testnet blockchain](https://testnets.opensea.io/collection/squarenft-0p0kmsggmh)